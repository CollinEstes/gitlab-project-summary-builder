# gitlab-project-summary-builder

## What is the problem?

Gitlab Utlimate provides all the core resources to effectively manage software projects with an agile methodology: Epics, Milestones, Iterations and Issues.  These resources, and their associated metadata, are used to create project roadmap dashboards and other management views that give software managers the ability to view project health and status in real time.

However, within organizations where executive leadership's focus is outside the software domain, where software is an enabling facet but not the core focus of the business, adding Gitlab to the already long list of software systems utilized by executive leadership is a burden.  This reality in practice leads to software managers having to invest large amounts of manual time pulling data out of Gitlab and into a form more suited to the nominal toolchain for executives: Word, Excel, Powerpoint and PDFs. These work products are not only slow to produce, the quality of the data in these reports is quickly stale with no automated means to update.

## What can solve this problem?

We can create a simple program that takes the data out of Gitlab, which is stored across several different related data resources, and combines it into a single "executive summary" file, in a format conducive to the nominal executive tools, and that can be easily re-generated with the latest data.

## How can we build this solution?

Gitlab provides a well documented web-API that allows for a software program to query and "pull" the project management data resources that reside within the Gitlab Application.  These resources can be put together into a single presentation object that can written to a file and then dissemenated via email or messaging system.

In short our solution will:

1. Pull data out of Gitlab
2. Combine and format data into a single "summary" data object
3. Write that data into a file (markdown format file)
4. Convert the markdown file into a nominal distrubutable format (pdf) 
5. Allow for the file to be downloaded

### Technologies

? I am thinking Go-lang
